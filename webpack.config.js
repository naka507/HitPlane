const path = require('path');
const uglifyjsPlugin = require('uglifyjs-webpack-plugin');
const htmlPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        entry: './src/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.js'
    },
    module: {

        rules: [
            {
                test: /\.json$/,
                loader: 'file-loader?name=resource/[name].[ext]'
            },
            {
                test: /\.(png|jpg|gif)/,
                loader: 'file-loader?limit=8192&name=images/[name].[ext]'
            },
            {
                test: /\.(mp3)/,
                loader: 'file-loader?limit=8192&name=audio/[name].[ext]'
            },
            {
                test: /\.(jsx|js)$/,
                use: {
                    loader: 'babel-loader'
                },
                exclude: /node_modules/
            }
        ]

    },
    plugins: [
        new uglifyjsPlugin({
            mangle: {
                except: ['$super', '$', 'exports', 'require']
            }
        }),
        new htmlPlugin({
            minify : {
                removerAttributeQuotes : true
            },
            hash : true,
            template :'./src/index.html'
        })
    ],
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        host: '127.0.0.1',
        compress: true,
        port: 80
    }
}