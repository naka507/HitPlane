import Game from './js/game'
import Player     from './js/player/index'
import Enemy      from './js/npc/enemy'
import BackGround from './js/runtime/background'
import GameInfo   from './js/runtime/gameinfo'
import Music      from './js/runtime/music'
import DB    from './js/databus'

let canvas = document.getElementById("app");
let ctx   = canvas.getContext('2d')
let databus = new DB()

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

window.canvas = canvas
window.Player = Player
window.Enemy = Enemy
window.BackGround = BackGround
window.GameInfo = GameInfo
window.Music = Music
window.databus = databus
window.ctx = ctx

let Symbol  = window.Symbol
let idCounter = 0

if (!Symbol) {
  Symbol = function Symbol(key) {
    return `__${key}_${Math.floor(Math.random() * 1e9)}_${++idCounter}__`
  }

  Symbol.iterator = Symbol('Symbol.iterator')
}

window.Symbol = Symbol

require('./images/Common.png')
require('./images/explosion1.png')
require('./images/bullet.png')
require('./images/enemy.png')
require('./images/hero.png')
require('./images/bg.jpg')
require('./audio/bgm.mp3')
require('./audio/boom.mp3')
require('./audio/bullet.mp3')

for (let i = 1; i <= 19; i++) {
    require(`./images/explosion${i}.png`)  
}

document.getElementById("start").onclick = function(){
  document.getElementById("bgstart").style.display = "none";
  new Game()
};
